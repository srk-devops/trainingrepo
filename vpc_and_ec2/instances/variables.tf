variable "region" {
  default = "us-east-1"
  description = "this is the default region"
}

variable "remote_state_bucket" {
  description = "Bucket namne of our demo VPC"
}

variable "remote-state_key" {
  description = "state file and name with absolute path"
}

variable "ec2_instance_type" {
  description = "Type of instance"
}

variable "key_pair_name" {
  description = "Key Pair Name"
}

variable "max_instance_size" {
  description = "maximum number of instances that scale up to"
}

variable "min_instance_size" {
  description = "min number of instances required"
}