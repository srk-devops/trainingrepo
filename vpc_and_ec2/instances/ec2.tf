#Define Cloud Service Provider
provider "aws" {
  region = "${var.region}"
}

#Define Source VPC information from S3
data "terraform_remote_state" "demo_vpc_config" {
  backend = "s3"
  config = {
    bucket = "${var.remote_state_bucket}"
    key = "${var.remote-state_key}"
    region = "${var.region}"
  }
}

#Define S3 Bucket to store Remote State of Instances which includes AutoScaling Group, Launch COnfiguration, Security Gorups, IAM Roles, IAM Polices, ELB, ELB Role, ELB Policy, ELB Security Group
terraform {
  backend "s3" {
    bucket = "precisein"
    key    = "test/demo.prject-instance.tfstate"
    region = "us-east-1"
  }
}

#Define Security group for Public Subnet
resource "aws_security_group" "ec2_public_sg" {
  name = "EC2-Public-sg"
  description = "Internet access to front end servers"
  vpc_id = "${data.terraform_remote_state.demo_vpc_config.outputs.vpc_id}"

  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    protocol = "TCP"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Define Security group for private subnet instances
resource "aws_security_group" "ec2_private_sg" {
  name = "EC2-Public-sg"
  description = "Internet access to backend servers"
  vpc_id = data.terraform_remote_state.demo_vpc_config.outputs.vpc_id

  ingress {
    from_port = 0
    protocol = "TCP"
    to_port = 0
    cidr_blocks = ["${aws_security_group.ec2_public_sg.id}"]
    #security_groups = ["${aws_security_group.ec2_public_sg.id}"]
  }

  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow health check for instances using this SG"
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Define security Group for Load Balancer
resource "aws_security_group" "elb_sg" {
  name = "elb_security_group"
  description = "Elb security group"
  vpc_id = "${data.terraform_remote_state.demo_vpc_config.outputs.vpc_id}"

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow web traffic to loadbalancer"
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Define IAM Rile for Autoscaling
resource "aws_iam_role" "ec2_iam_role" {
  name = "EC2-IAM-Role"
  assume_role_policy = <<EOF
 {
    "Version" : "2012-10-17",
    "Statement" :
   [
     {
     "Effect" : "Allow",
     "Principal" : {
       "Service" : [ "ec2.amazonaws.com", "application-autoscaling.amazonaws.com" ]
     }
     "Action" : "sts:AssumeRole"
     }
   ]
 }
  EOF
}

#Define Policy for ec2
resource "aws_iam_role_policy" "ec2_iam_role_policy" {
  name = "EC2-IAM-Policy"
  role = "${aws_iam_role.ec2_iam_role.id}"
  policy = <<EOF
{
  "Version" : "2012-10-17",
  "Statement" : [
    {
      "Effect" : "Allow",
       "Action" : [
         "ec2:*",
         "elasticloadbalancing:*",
         "cloudwatch:*",
         "logs:*"
        ],
        "Resources": "*"
    }
  ]
}
EOF
}

# Associate created IAM Role to instance profile
resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "EC2-IAM-Instance-Profile"
  role = "${aws_iam_role.ec2_iam_role.name}"
}

#Create Lauch configuration
data "aws_ami" "launch_configuration_ami" {
  most_recent = true

  filter {
    name = "owner-alias"
    values = ["amazon"]
  }
  owners = ["amazon"]
}

#Define Lauch configuration for Private instances/apps
resource "aws_launch_configuration" "ec2_private_launch_configuration" {
  image_id = "${data.aws_ami.launch_configuration_ami.id}"
  instance_type = "${var.ec2_instance_type}"
  key_name = "${var.key_pair_name}"
  associate_public_ip_address = false
  iam_instance_profile = "${aws_iam_instance_profile.ec2_instance_profile.name}"
  security_groups = ["${aws_security_group.ec2_private_sg.id}"]

  user_data = <<EOF
     #!/bin/bash
      yum update -y
      yum install httpd24 -y
      service httpd start
      chkconfig httpd on
      export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
      echo "<html><body><h1> Hello from demo private backend at instance <b>"$INSTANCE_ID"</b></body></html>" > /var/www/html/index.html
   EOF
}

#Define Lauch configuration for Public instances/apps
resource "aws_launch_configuration" "ec2_public_launch_configuration" {
  image_id = "${data.aws_ami.launch_configuration_ami.id}"
  instance_type = "${var.ec2_instance_type}"
  key_name = "${var.key_pair_name}"
  associate_public_ip_address = true
  iam_instance_profile = "${aws_iam_instance_profile.ec2_instance_profile.name}"
  security_groups = ["${aws_security_group.ec2_public_sg.id}"]

  user_data = <<EOF
     #!/bin/bash
      yum update -y
      yum install httpd24 -y
      service httpd start
      chkconfig httpd on
      export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)
      echo "<html><body><h1> Hello from demo public frontend wepadd at instance <b>"$INSTANCE_ID"</b></body></html>" > /var/www/html/index.html
   EOF
}

#Create elb for front end apps
resource "aws_elb" "webapp_load_balancer" {
  name = "demo-webapp-elb"
#  internal = flase
  security_groups = ["${aws_security_group.elb_sg.id}"]
  subnets = [
        "${data.terraform_remote_state.demo_vpc_config.outputs.public-subnet-1a-id}",
        "${data.terraform_remote_state.demo_vpc_config.outputs.public-subnet-1b-id}",
        "${data.terraform_remote_state.demo_vpc_config.outputs.public-subnet-1c-id}",
  ]
  listener {
    instance_port = 80
    instance_protocol = "HTTP"
    lb_port = 80
    lb_protocol = "HTTP"
  }
  health_check {
    healthy_threshold = 5
    interval = 30
    target = "HTTP:80/index.html"
    timeout = 10
    unhealthy_threshold = 5
  }
}

#Create ELB for backend apps
resource "aws_elb" "backend_load_balancer" {
  name = "backend-elb"
  internal = true
  security_groups = ["${aws_security_group.elb_sg.id}"]
  subnets = [
    "${data.terraform_remote_state.demo_vpc_config.outputs.private-subnet-1a-id}",
    "${data.terraform_remote_state.demo_vpc_config.outputs.private-subnet-1b-id}",
    "${data.terraform_remote_state.demo_vpc_config.outputs.private-subnet-1c-id}"
  ]
  listener {
    instance_port = 80
    instance_protocol = "HTTP"
    lb_port = 80
    lb_protocol = "HTTP"
  }

  health_check {
    healthy_threshold = 5
    interval = 30
    target = "HTTP:80/index.html"
    timeout = 10
    unhealthy_threshold = 5
  }
}

#Create AutoScaling Group for Privte instances in private subnet
resource "aws_autoscaling_group" "ec2_private_autoscaling_group" {
  name = "backend-autoscalinggroup"
  vpc_zone_identifier = [
    "${data.terraform_remote_state.demo_vpc_config.outputs.private-subnet-1a-id}",
    "${data.terraform_remote_state.demo_vpc_config.outputs.private-subnet-1b-id}",
    "${data.terraform_remote_state.demo_vpc_config.outputs.private-subnet-1c-id}"
  ]
  max_size = "${var.max_instance_size}"
  min_size = "${var.min_instance_size}"
  launch_configuration = "${aws_launch_configuration.ec2_private_launch_configuration.name}"
  health_check_type = "ELB"
  load_balancers = ["${aws_elb.backend_load_balancer.name}"]

  tag {
    key = "Name"
    propagate_at_launch = false
    value = "Backend-EC2-Instances"
  }
  tag {
    key = "Type"
    propagate_at_launch = false
    value = "Production"
  }
}

#Create AutoScaling Group for Privte instances in private subnet
resource "aws_autoscaling_group" "ec2_public_autoscaling_group" {
  name = "frontend-autoscalinggroup"
  vpc_zone_identifier = [
    "${data.terraform_remote_state.demo_vpc_config.outputs.public-subnet-1a-id}",
    "${data.terraform_remote_state.demo_vpc_config.outputs.public-subnet-1b-id}",
    "${data.terraform_remote_state.demo_vpc_config.outputs.public-subnet-1c-id}"
  ]
  max_size = "${var.max_instance_size}"
  min_size = "${var.min_instance_size}"
  launch_configuration = "${aws_launch_configuration.ec2_public_launch_configuration.name}"
  health_check_type = "ELB"
  load_balancers = ["${aws_elb.webapp_load_balancer.name}"]

  tag {
    key = "Name"
    propagate_at_launch = false
    value = "Backend-EC2-Instances"
  }

  tag {
    key = "Type"
    propagate_at_launch = false
    value = "Production"
  }
}

#target tracking scaling
resource "aws_autoscaling_policy" "webapp_scaling_policy" {
  autoscaling_group_name = "${aws_autoscaling_group.ec2_public_autoscaling_group.name}"
  name = "webapp-Autoscaling-policy"
  policy_type = "TargetTrackingScaling"
  min_adjustment_magnitude = 1

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 80.0
  }
}

#target tracking scaling
resource "aws_autoscaling_policy" "backend_scaling_policy" {
  autoscaling_group_name = "${aws_autoscaling_group.ec2_private_autoscaling_group.name}"
  name = "backend-Autoscaling-policy"
  policy_type = "TargetTrackingScaling"
  min_adjustment_magnitude = 1

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }
    target_value = 80.0
  }
}

#Create a SNS Topic for public instances in public subnet
resource "aws_sns_topic" "webapp_autoscaling_alert_topic" {
  display_name = "webapp-autoscaling-topic"
  name = "webapp-autoscaling-topic"
}

#Subscription
resource "aws_sns_topic_subscription" "webapp-autoscaling-sms-subscription" {
  endpoint = "+919290128288"
  protocol = "sms"
  topic_arn = "${aws_sns_topic.webapp_autoscaling_alert_topic.arn}"
}

#
resource "aws_autoscaling_notification" "webapp-autoscaling-notification" {
  group_names = ["${aws_autoscaling_group.ec2_public_autoscaling_group.name}"]
  notifications = [
      "autoscaling:EC2_INSTANCE_LAUNCH",
      "autoscaling:EC2_INSTANCE_TERMINATE",
      "autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
  ]
  topic_arn = "${aws_sns_topic.webapp_autoscaling_alert_topic.arn}"
}

resource "aws_sns_topic" "backend_autoscaling_alert_topic" {
  display_name = "backend-autoscaling-topic"
  name = "backend-autoscaling-topic"
}

#Subscription
resource "aws_sns_topic_subscription" "backend-autoscaling-sms-subscription" {
  endpoint = "+919290128288"
  protocol = "sms"
  topic_arn = "${aws_sns_topic.backend_autoscaling_alert_topic.arn}"
}

#
resource "aws_autoscaling_notification" "backend-autoscaling-notification" {
  group_names = ["${aws_autoscaling_group.ec2_private_autoscaling_group.name}"]
  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
  ]
  topic_arn = "${aws_sns_topic.backend_autoscaling_alert_topic.arn}"
}
