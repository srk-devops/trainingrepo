#Define our cloud provider name
#provider "aws" {
 # region = var.region

  #version     = "~> 3.0"
  #version = "~> 2.25.0"
#  shared_credentials_file = "/Users/ramakrishna.s/.aws/credentials"
  #profile = "srk"
#}

provider "aws" {
  region                  = "${var.region}"
  #shared_credentials_file = "/Users/ramakrishna.s/.aws/credentials"
  profile                 = "srk"
}


terraform {
  backend "s3" {
    bucket = "precisein"
    key    = "test/demo.prject-infra.tfstate"
    region = "us-east-1"
  }
}
#Define backend configuration
#data "terraform_remote_state" "demo-statefile" {
#backend = "s3"
#config = {
#bucket = "prataphdemo"
#key = "test/demo.prject-infra.tfstate"
#region = "us-east-1"
#  }
#}

# Define VPC for our demo with name demo-vpc
resource "aws_vpc" "demo-vpc" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags = {
    Name = "demo-vpc"
  }
}

#define subnet
resource "aws_subnet" "public-subnet-1a" {
  cidr_block        = "${var.pub-sub-cidr-1}"
  vpc_id            = "${aws_vpc.demo-vpc.id}"
  availability_zone = "us-east-1a"

  tags = {
    Name = "public-subnet-1a"
  }
}

resource "aws_subnet" "public-subnet-1b" {
  cidr_block        = "${var.pub-sub-cidr-2}"
  vpc_id            = "${aws_vpc.demo-vpc.id}"
  availability_zone = "us-east-1b"

  tags = {
    Name = "public-subnet-1b"
  }
}

resource "aws_subnet" "public-subnet-1c" {
  cidr_block        = "${var.pub-sub-cidr-3}"
  vpc_id            = "${aws_vpc.demo-vpc.id}"
  availability_zone = "us-east-1c"

  tags = {
    Name = "public-subnet-1c"
  }
}

resource "aws_subnet" "private-subnet-1a" {
  cidr_block        = "${var.prv-sub-cidr-1}"
  vpc_id            = "${aws_vpc.demo-vpc.id}"
  availability_zone = "us-east-1a"

  tags = {
    Name = "private-subnet-1a"
  }
}

resource "aws_subnet" "private-subnet-1b" {
  cidr_block        = "${var.prv-sub-cidr-2}"
  vpc_id            = "${aws_vpc.demo-vpc.id}"
  availability_zone = "us-east-1b"

  tags = {
    Name = "private-subnet-1b"
  }
}

resource "aws_subnet" "private-subnet-1c" {
  cidr_block        = "${var.prv-sub-cidr-3}"
  vpc_id            = "${aws_vpc.demo-vpc.id}"
  availability_zone = "us-east-1c"

  tags = {
    Name = "private-subnet-1c"
  }
}

#Define public and Private route tables

# Public Route Table
resource "aws_route_table" "public-route-table" {
  vpc_id = "${aws_vpc.demo-vpc.id}"

  tags = {
    Name = "public route table"
  }
}

# Private Route Table
resource "aws_route_table" "private-route-table" {
  vpc_id = "${aws_vpc.demo-vpc.id}"

  tags = {
    Name = "private route table"
  }
}

#Associate the public subnet with route tables
resource "aws_route_table_association" "public-subnet-1a-association" {
  route_table_id = "${aws_route_table.public-route-table.id}"
  subnet_id      = "${aws_subnet.public-subnet-1a.id}"
}

resource "aws_route_table_association" "public-subnet-1b-association" {
  route_table_id = "${aws_route_table.public-route-table.id}"
  subnet_id      = "${aws_subnet.public-subnet-1b.id}"
}

resource "aws_route_table_association" "public-subnet-1c-association" {
  route_table_id = "${aws_route_table.public-route-table.id}"
  subnet_id      = "${aws_subnet.public-subnet-1c.id}"
}

#Associate the private subnet with route tables
resource "aws_route_table_association" "private-subnet-1a-association" {
  route_table_id = "${aws_route_table.private-route-table.id}"
  subnet_id      = "${aws_subnet.private-subnet-1a.id}"
}

resource "aws_route_table_association" "private-subnet-1b-association" {
  route_table_id = "${aws_route_table.private-route-table.id}"
  subnet_id      = "${aws_subnet.private-subnet-1b.id}"
}

resource "aws_route_table_association" "private-subnet-1c-association" {
  route_table_id = "${aws_route_table.private-route-table.id}"
  subnet_id      = "${aws_subnet.private-subnet-1c.id}"
}

#Create Elastic IP for NAT Gateway
resource "aws_eip" "elasticIP-for-NatGw" {
  vpc                       = true
  associate_with_private_ip = "10.0.0.5"

  tags = {
    Name = "Demo project EIP"
  }
}



#Create NAT Gateway --> requires elatic ip and one of the public subnet associated
resource "aws_nat_gateway" "demo-NatGw" {
  allocation_id = "${aws_eip.elasticIP-for-NatGw.id}"
  subnet_id     = "${aws_subnet.public-subnet-1a.id}"

  tags = {
    Name = "Demo-Nat-Gw"
  }

  depends_on = ["aws_eip.elasticIP-for-NatGw"]
}

#Associate Route Table for NatGw to enable routing/access to internet (Egress only)
resource "aws_route" "demo-NatGw-route" {
  route_table_id         = "${aws_route_table.private-route-table.id}"
  nat_gateway_id         = "${aws_nat_gateway.demo-NatGw.id}"
  destination_cidr_block = "0.0.0.0/0"
}

#Create Internet Gateway
resource "aws_internet_gateway" "demo-iGw" {
  vpc_id = "${aws_vpc.demo-vpc.id}"

  tags = {
    Name = "demo-iGw"
  }
}

#Associate Route Table
resource "aws_route" "demo-iGw-route" {
  route_table_id         = "${aws_route_table.public-route-table.id}"
  gateway_id             = "${aws_internet_gateway.demo-iGw.id}"
  destination_cidr_block = "0.0.0.0/0"
}

