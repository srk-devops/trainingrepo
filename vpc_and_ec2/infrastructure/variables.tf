variable "region" {
  default     = "us-east-1"
  description = "default demo project location"
}

variable "vpc_cidr" {
  #  default = "10.0.0.0/16"
  description = "default demo project vpc CIDR block"
}

variable "pub-sub-cidr-1" {
  description = "cidr for public subnet 1"
}

variable "pub-sub-cidr-2" {
  description = "cidr for public subnet 2"
}

variable "pub-sub-cidr-3" {
  description = "cidr for public subnet 3"
}

variable "prv-sub-cidr-1" {
  description = "cidr for private subnet 1"
}

variable "prv-sub-cidr-2" {
  description = "cidr for private subnet 2"
}

variable "prv-sub-cidr-3" {
  description = "cidr for private subnet 3"
}

